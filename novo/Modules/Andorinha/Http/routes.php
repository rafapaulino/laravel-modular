<?php

Route::group(['middleware' => 'web', 'prefix' => 'andorinha', 'namespace' => 'Modules\Andorinha\Http\Controllers'], function()
{
    Route::get('/', 'AndorinhaController@index');
});
