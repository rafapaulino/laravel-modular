<?php

Route::group(['middleware' => 'web', 'prefix' => 'ze', 'namespace' => 'Modules\Ze\Http\Controllers'], function()
{
    Route::get('/', 'ZeController@index');
});
