<?php

Route::group(['middleware' => 'web', 'prefix' => 'gaivota', 'namespace' => 'Modules\Gaivota\Http\Controllers'], function()
{
    Route::get('/', 'GaivotaController@index');
});
