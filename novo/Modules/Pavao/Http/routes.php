<?php

Route::group(['middleware' => 'web', 'prefix' => 'pavao', 'namespace' => 'Modules\Pavao\Http\Controllers'], function()
{
    Route::get('/', 'PavaoController@index');
});
