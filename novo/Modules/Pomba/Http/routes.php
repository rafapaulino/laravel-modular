<?php

Route::group(['middleware' => 'web', 'prefix' => 'pomba', 'namespace' => 'Modules\Pomba\Http\Controllers'], function()
{
    Route::get('/', 'PombaController@index');
});
